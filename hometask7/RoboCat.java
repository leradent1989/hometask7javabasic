package hometask7;


import java.util.HashSet;
import java.util.Set;

public class RoboCat extends Pet implements PetInterface{




    public RoboCat(String nickName,int age,byte trickLevel) {

        super( nickName, age, trickLevel);

        this.species =  Species.ROBOCAT;

    }

    public void respond() {
        System.out.println("Привет хозяин Я " + species+ " "+ this.getNickName() + " рад видеть Вас");
    }

    public void foul(){
        System.out.println("Роботы не делают глупостей ...");
    }



    public static void main(String[] args) {
        RoboCat K5 = new RoboCat("Mimi",5,(byte) 70);
        HashSet<String> habits = new HashSet<>(Set.of("serve","cook dinner","massage","speak"));
        K5.setHabits(habits);
        K5.toString();
        K5.respond();
        K5.foul();
        System.out.println(K5.getSpecies());
    }

}

