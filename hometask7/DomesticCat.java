package hometask7;

import java.util.HashSet;
import java.util.Set;

public class DomesticCat extends Pet implements PetInterface{




    public DomesticCat(String nickName,int age,byte trickLevel) {
        super( nickName, age, trickLevel);
        this.species = Species.DOMESTICCAT;

    }

    public void respond() {
        System.out.println("Привет хозяин Я " + species+ " "+ this.getNickName() + " cоскучилcя");
    }

    public void foul(){
        System.out.println("Поточу ка я когти об этот диван ...");
    }



    public static void main(String[] args) {
        DomesticCat Mura = new DomesticCat("Mura",5,(byte) 70);
        HashSet<String> habits = new HashSet<>(Set.of("sleep","drink","eat"));
        Mura.setHabits(habits);
        Mura.toString();
        Mura.respond();
        Mura.foul();
        System.out.println(Mura.getSpecies());
    }

}

