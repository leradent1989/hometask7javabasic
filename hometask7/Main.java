package hometask7;

import java.time.DayOfWeek;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        HashSet <String> habits = new HashSet<>(Set.of("sleep","drink","eat"));
        DomesticCat Mura = new DomesticCat("Mura",5,(byte) 70);
        Mura.setHabits(habits);
        Mura.toString();
        Mura.respond();
        Mura.foul();
        Dog  snoopy = new Dog("Mimi",5,(byte) 30);
        snoopy.setHabits(habits);
        snoopy.setNickName("snoopy");
        snoopy.toString();
        snoopy.respond();
        RoboCat K5 = new RoboCat("Mimi",5,(byte) 70);
        K5.toString();
        K5.respond();
        K5.foul();
        HashMap<DayOfWeek,String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY,"brush my teeth");
        schedule.put(DayOfWeek.TUESDAY,"do exercises");
        schedule.put(DayOfWeek.WEDNESDAY,"go for a walk");
        schedule.put(DayOfWeek.THURSDAY,"pass exams");
        schedule.put(DayOfWeek.FRIDAY,"go to night club");
        schedule.put(DayOfWeek.SATURDAY,"bake cake");
        schedule.put(DayOfWeek.SUNDAY,"rest well,watch films");
        Woman Mary = new Woman();
        Mary.setSchedule(schedule);
        System.out.println(Mary.doMakeUp("Done"));
        Mary.toString();
        Woman mother = new Woman();
        mother.setSchedule(schedule);
        Man father = new Man();
        father.setSchedule(schedule);
        Family smith = new Family(mother,father);
        Mary.setFamily(smith);
        Pet Mimi = new Dog("Mimi",5,(byte) 60);
        Mimi.setHabits(habits);
        Mimi.toString();
        Set pet =new HashSet<>(Set.of(Mimi));
        smith.setPet(pet);
        Mary.greetPet(Mimi);
        Man George = new Man();
        System.out.println(George.fixCar("Fixed"));

    }
}
